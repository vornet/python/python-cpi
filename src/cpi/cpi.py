from __future__ import annotations
import collections
import sys
from typing import (
    Any,
    Callable,
    Generic,
    Iterator,
    List,
    Optional,
    TypeVar,
    Union,
)


ItemT = TypeVar('ItemT')
DebugFnT = Callable[[str], None]
BoolFnT = Callable[[Any], bool]
FormatFnT = Callable[[Any], str]
TranslateFnT = Callable[[ItemT], Any]
FoldFnT = Callable[[ItemT, ItemT], ItemT]
InitFnT = Callable[[], ItemT]
VarargFnT = Callable

ColT = TypeVar('ColT', bound='Col')
GenT = TypeVar('GenT', bound='Gen')

ItemColT = List[ItemT]
ItemGenT = Iterator[ItemT]


class VOID:
    """
    A special value indicating nothing

    Use this if None already means something in your
    collections.
    """
    pass


class INF:
    """
    A special value indicating infinity
    """


class ANY:
    """
    A special value indicating any value
    """


class ScalarError(ValueError):

    def __init__(self, message: str, col: Union[ColT, GenT]):
        self.message = message
        self.col = col

    def __str__(self):
        return self.message


class ValidationError(ValueError):
    pass


def debug(msg: str):
    sys.stderr.write(msg + '\n')


class Col(Generic[ItemT]):
    """
    A list wrapper that adds several methods that allow to easily chain
    operations into pipelines as described in Martin Fowler's Collection
    Pipeline article:

        https://www.martinfowler.com/articles/collection-pipeline/#FirstEncounters

    Almost the same as Gen but internally uses lists, which means that the
    memory footprint is probably larger but collections are re-usable.

    Every method will return new instance of Col with items altered as needed.
    This allows for easy chaining of collections.

    Col itself does implement iteration but for convenience, items can be
    retrieved as list using *items* property.

        >>> C = Col('abc').foreach(str.upper)
        >>> for c in C:
        ...     print(c)
        A
        B
        C
        >>> C.items
        ['A', 'B', 'C']

    Properties *first*, *last* and *only* can be used as "hidden" asserts:
    they are guarranteed to return single collection members but raise
    ScalarError if the collection itself is empty or -- in case of *only*,
    if it has more than one element.

        >>> Col('abc').only
        Traceback (most recent call last):
            ...
        cpi.ScalarError: item list is not a singleton: 3 items

    Additionally, properties *as_list*, *as_tuple* and *as_dict* can be
    used to retrieve list, tuple or dict constructed from the items in
    the collection.

        >>> Col('abc').as_tuple
        ('a', 'b', 'c')

    For example:

        >>> L = [1, 2, 3, 100, 200, 300]
        >>> Col(L).foreach(str).items
        ['1', '2', '3', '100', '200', '300']
        >>> Col(L).foreach(str).foreach(tuple).column(0).join_by('.').items
        ['1.2.3.1.2.3']
        >>> Col(L).foreach(str).select(lambda s: s.endswith('0')).items
        ['100', '200', '300']
        >>> Col([-5, None, 5]).default(lambda: 0).items
        [-5, 0, 5]
    """

    def __init__(self,
                 items: ItemColT,
                 debugfn: Optional[DebugFnT] = None,
                 ):
        self.items = items
        self._debugfn: DebugFnT = debugfn or debug

    def __iter__(self) -> Iterator:
        return iter(self.items)

    def _clone(self: ColT,
               items: ItemColT,
               ) -> ColT:
        return self.__class__(
            items=items,
            debugfn=self._debugfn,
        )

    #
    # retrieval
    #

    @property
    def as_dict(self: ColT) -> dict:
        """
        Return all items as a dictionary

        Example:

            >>> Col('abc').foreach(lambda i: (i, i.upper())).as_dict
            {'a': 'A', 'b': 'B', 'c': 'C'}
        """
        return dict(i for i in self)

    @property
    def as_list(self: ColT) -> list:
        """
        Return all items as a list

        Example:

            >>> Col('abc').foreach(str.upper).as_list
            ['A', 'B', 'C']
        """
        return [i for i in self]

    @property
    def as_set(self: ColT) -> set:
        """
        Return all items converted to a set

        Example:

            >>> S = Col('abca').as_set
            >>> type(S)
            <class 'set'>
            >>> sorted(S)
            ['a', 'b', 'c']
        """
        return set(i for i in self)

    @property
    def as_tuple(self: ColT) -> tuple:
        """
        Return all items as a tuple

        Example:

            >>> Col('abc').foreach(str.upper).as_tuple
            ('A', 'B', 'C')
        """
        return tuple(i for i in self)

    #
    # inspection
    #

    def debug(self: ColT,
              tag: str = '',
              fn: Optional[DebugFnT] = None,
              ) -> ColT:
        """
        For every item, emit debug message using fn or self.debugfn

        Example:

            >>> C = Col("foo bar baz".split(), debugfn=print)
            >>> C.debug('THREEs').items
            THREEs|'foo'
            THREEs|'bar'
            THREEs|'baz'
            ['foo', 'bar', 'baz']
        """
        _fn: DebugFnT = fn or self._debugfn
        for item in self.items:
            _fn('%s|%r' % (tag, item))
        return self

    def validate(self: ColT,
                 fn: BoolFnT,
                 fmt: Optional[FormatFnT] = None,
                 ) -> ColT:
        """
        Emit collection composed of identical items or raise ValidationError.

        If for any item, fn returns False, raise ValidationError with the given
        item.  The argument to the exception is the "bad" item itself, unless
        *fmt* callable is provided; in that case the ValidationError will
        hold `fmt(item)`.

        Example:

            >>> C = Col([3, 2, 1])
            >>> C.validate(fn=lambda i: i<10).items
            [3, 2, 1]
            >>> C.validate(fn=lambda i: i>1).items
            Traceback (most recent call last):
                ...
            cpi.ValidationError: 1
            >>> C.validate(
            ...     fn=lambda i: i>1,
            ...     fmt=lambda i: "too low: %d" % i
            ... ).items
            Traceback (most recent call last):
                ...
            cpi.ValidationError: too low: 1
        """
        _fmt: FormatFnT = fmt or (lambda i: i)
        for item in self.items:
            if fn(item):
                continue
            raise ValidationError(_fmt(item))
        return self._clone(self.items)

    #
    # scalars
    #

    @property
    def first(self: ColT) -> ItemT:
        """
        Return the first member of collection or raise ScalarError

        If the collection is empty, a ScalarError is raised with *col*
        attribute set to refer to the collection.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.first
            1
            >>> E = Col([])
            >>> E.first
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        if len(self.items) == 0:
            raise ScalarError(message="item list is empty", col=self)
        return self.items[0]

    @property
    def last(self: ColT) -> ItemT:
        """
        Return the last member of collection or raise ScalarError

        If the collection is empty, a ScalarError is raised with *col*
        attribute set to refer to the collection.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.last
            3
            >>> E = Col([])
            >>> E.last
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        if len(self.items) == 0:
            raise ScalarError(message="item list is empty", col=self)
        return self.items[-1]

    @property
    def only(self: ColT) -> ItemT:
        """
        Return the only member of collection or raise ScalarError

        If the number of items in the collection is different than one,
        a ScalarError is raised with *col* attribute set to the current
        collection, so that it can be re-raised or dealt with depending
        on actual number of items.

        Example:

            >>> C = Col([1])
            >>> C.only
            1
            >>> C.prepend(2).only
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is not a singleton: 2 items
            >>> C.erase().only
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        if len(self.items) == 0:
            raise ScalarError("item list is empty", col=self)
        if len(self.items) > 1:
            raise ScalarError("item list is not a singleton: %d items"
                              % len(self.items),
                              col=self)
        return self.items[0]

    #
    # slicing
    #

    def head(self: ColT,
             n: int = 1,
             ) -> ColT:
        """
        Emit collection of n first items

        By default, just first item is preserved.

        *n* can be INF, in which case all items are
        preserved.

        Example:

            >>> C = Col([1, 2, 3, 4, 5, 6])
            >>> C.head(2).items
            [1, 2]
            >>> C.head().items
            [1]
            >>> C.head(INF).items
            [1, 2, 3, 4, 5, 6]
        """
        if n == INF:
            return self._clone(self.items[:])
        return self._clone(self.items[0:n])

    def tail(self: ColT,
             n: int = 1,
             ) -> ColT:
        """
        Emit collection of n last items

        By default, one last item is preserved.

        *n* can be INF, in which case all items are
        preserved.

        Example:

            >>> C = Col([1, 2, 3, 4, 5, 6])
            >>> C.tail(2).items
            [5, 6]
            >>> C.tail().items
            [6]
            >>> C.tail(INF).items
            [1, 2, 3, 4, 5, 6]
        """
        if n == INF:
            return self._clone(self.items[:])
        return self._clone(self.items[-n:])

    #
    # chaining
    #

    def flatten(self: ColT) -> ColT:
        """
        Flatten all lists in self.item; return collection of longer item list.

        Example:

            >>> C = Col([[1, 2], [3, 4], [5, 6]])
            >>> C.flatten().items
            [1, 2, 3, 4, 5, 6]
        """
        joint = self.reduce(func=lambda a, b: a+b, init=list).items
        return self._clone(joint[0])

    def append(self: ColT,
               item: ItemT,
               ) -> ColT:
        """
        Emit collection with extra item *item* appended at the end.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.append(4).items
            [1, 2, 3, 4]
        """
        return self._clone(self.items + [item])

    def prepend(self: ColT,
                item: ItemT,
                ) -> ColT:
        """
        Emit collection with extra item *item* pre-pended at the beginning.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.prepend(0).items
            [0, 1, 2, 3]
        """
        return self._clone([item] + self.items)

    def windows(self: ColT,
                size: int,
                pad: Optional[ItemT] = None,
                ) -> ColT:
        """
        Emit collection of *size* long slices of the original collection.

        Windows are padded using *pad* value so that the length of every
        window is guarranteed to be *size*.  If None already means something
        in your collection, you can set *pad* to something else, eg. col.VOID.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.windows(2).items
            [[1, 2], [2, 3], [3, None]]
            >>> C.windows(0).items
            Traceback (most recent call last):
                ...
            ValueError: size must be at least 1, got: 0
            >>> C.windows(1).items
            [[1], [2], [3]]
            >>> C = Col([None, 2, 3])
            >>> C.windows(2, pad=VOID).items
            [[None, 2], [2, 3], [3, <class 'cpi.cpi.VOID'>]]
        """
        out = []
        togo = self.items.copy()
        if size < 1:
            raise ValueError(f"size must be at least 1, got: {size!r}")
        while togo:
            padding = size * [pad]
            win = (togo + padding)[0:size]
            out.append(win)
            togo.pop(0)
        return self._clone(out)

    #
    # filtering
    #

    def column(self: ColT,
               idx: Any,
               ) -> ColT:
        """
        Emit collection composed of sub-members of every item at index *idx*.

        Example:

            >>> C = Col([(1, 2, 3), (100, 200, 300)])
            >>> C.column(1).items
            [2, 200]
            >>> C = Col(["foo", "bar", "baz"])
            >>> C.column(2).items
            ['o', 'r', 'z']
        """
        return self._clone([i[idx] for i in self.items])

    def erase(self: ColT) -> ColT:
        """
        Emit empty collection

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.erase().items
            []
        """
        return self._clone([])

    def select(self: ColT,
               func: BoolFnT,
               ) -> ColT:
        """
        Emit collection of items where *func* evaluates to True

        Example:

            >>> C = Col("foo.bar..baz".split('.'))
            >>> C.select(bool).items
            ['foo', 'bar', 'baz']
            >>> C = Col([1, 2, 3, 4])
            >>> C.select(func=lambda n: n > 2).items
            [3, 4]
        """
        return self._clone([i for i in self.items if func(i)])

    def tflt(self: ColT,
             pattern: tuple,
             asterisk: Any = ANY,
             ) -> ColT:
        """
        Emit collection of tuple-like items that match tuple pattern *pattern*.

        For every item of *pattern* tuple, respective item in self.items must
        be equal to the pattern item or to a special value of *cpi.ANY*.
        If *asterisk* is given, the provided value acts in place of *cpi.ANY*.

        Example:

            >>> data = []
            >>> data.append(("Joe", 36, "Seattle"))
            >>> data.append(("Jane", 23, "New York City"))
            >>> data.append(("Joe", 36, "Los Angeles"))
            >>> C = Col(data)
            >>> C.tflt(("Joe", 36, ANY)).items
            [('Joe', 36, 'Seattle'), ('Joe', 36, 'Los Angeles')]
            >>> C.tflt(("Joe", 36, None), asterisk=None).items
            [('Joe', 36, 'Seattle'), ('Joe', 36, 'Los Angeles')]
        """
        def match_cont(item, pattern):
            if len(item) != len(pattern):
                return False
            for idx in range(len(pattern)):
                if asterisk is None and pattern[idx] is None:
                    continue
                if pattern[idx] == asterisk:
                    continue
                if item[idx] == pattern[idx]:
                    continue
                return False
            return True
        out = []
        for item in self.items:
            if not match_cont(item, pattern):
                continue
            out.append(item)
        return self._clone(out)

    def reject(self: ColT,
               func: BoolFnT,
               ) -> ColT:
        """
        Emit collection of items where *func* evaluates to False

        This is exact inverse of *Col.select()*.

        Example:

            >>> C = Col([1, 2, 3, 4])
            >>> C.reject(func=lambda n: n > 2).items
            [1, 2]
        """
        return self._clone([i for i in self.items if not func(i)])

    def dedup(self: ColT,
              fn: Optional[TranslateFnT] = None,
              ) -> ColT:
        """
        Emit collection unique items of this collection.

        If *fn* is provided, instead of comparing items by their value, they
        are compared by value of this function.

        Example:

            >>> C = Col("foo.bar.baz.bar.foo".split('.'))
            >>> C.dedup().items
            ['foo', 'bar', 'baz']
            >>> C = Col("foo.bar.baz.bar.foo".split('.'))
            >>> C.dedup(lambda i: i[1]).items
            ['foo', 'bar']
        """
        _fn: TranslateFnT = fn or (lambda i: i)
        seen = []
        uniq = []
        for item in self.items:
            k = _fn(item)
            if k in seen:
                continue
            seen.append(k)
            uniq.append(item)
        return self._clone(uniq)

    #
    # sorting and ordering
    #

    def sort(self: ColT,
             fn: Optional[TranslateFnT] = None,
             ) -> ColT:
        """
        Emit same items in order sorted acording to fn.

        Example:

            >>> C = Col([3, 1, 2])
            >>> C.sort().items
            [1, 2, 3]
            >>> C = Col("axolotl foo hello".split())
            >>> C.sort(fn=len).items
            ['foo', 'hello', 'axolotl']
        """
        return self._clone(sorted(self.items, key=fn))

    def reverse(self: ColT) -> ColT:
        """
        Emit same items in reversed order.

        Example:

            >>> C = Col([1, 2, 3])
            >>> C.reverse().items
            [3, 2, 1]
        """
        return self._clone(self.items[::-1])

    def group_by(self: ColT,
                 fn: TranslateFnT,
                 ) -> ColT:
        """
        Emit collection of lists, self.items grouped according to *fn(item)*.

        Example:

            >>> C = Col("Joe Anne Bob Jane Betty".split())
            >>> C.group_by(lambda S: S[0]).items
            [['Joe', 'Jane'], ['Anne'], ['Bob', 'Betty']]
        """
        grouped = collections.defaultdict(list)
        for item in self.items:
            grouped[fn(item)].append(item)
        return self._clone(list(grouped.values()))

    #
    # member altering
    #

    def foreach(self: ColT,
                func: VarargFnT,
                *args: Any,
                **kwargs: Any,
                ):
        """
        Emit collection with every item replaced by result of *func(item)*.

        Example:

            >>> C = Col("Joe Anne Bob Jane Betty".split())
            >>> C.foreach(str.lower).items
            ['joe', 'anne', 'bob', 'jane', 'betty']
            >>> C.foreach(str.split, "n").items
            [['Joe'], ['A', '', 'e'], ['Bob'], ['Ja', 'e'], ['Betty']]
            >>> Person = collections.namedtuple('Person', 'name,age')
            >>> C.head(2).foreach(Person, age=30).items
            [Person(name='Joe', age=30), Person(name='Anne', age=30)]
        """
        return self._clone([func(i, *args, **kwargs) for i in self.items])

    def default(self: ColT,
                factory: InitFnT,
                ) -> ColT:
        """
        Emit collection of items with None re-generated using *factory*

        Example:

            >>> C = Col([3, 4, None, 6, 7])
            >>> C.default(lambda: 5).items
            [3, 4, 5, 6, 7]
        """
        def setdefault(i): return factory() if i is None else i
        return self._clone([setdefault(i) for i in self.items])

    #
    # folding
    #

    def reduce(self: ColT,
               func: FoldFnT,
               init: Optional[InitFnT] = None,
               ) -> ColT:
        """
        Emit collection of single item produced by folding on *func*

        First, *func* is called with first two items as positional arguments.
        Result of this call is then used for another call along with third
        item, etc. until the whole collection is exhausted.  Finally, return
        new collection with the last *func* result as its only item.

        If *init* is provided, it must be a function that accepts no arguments
        and the above process starts as if the first item in collection was the
        return value of *init*.

        If collection is empty, *init* result is used to construct the returned
        singleton collection.  If collection is empty and no *init* is given,
        ValueError is raised.

        Example:

            >>> import operator
            >>> C = Col([1, 10, 100])
            >>> C.reduce(operator.mul).items
            [1000]
            >>> C.erase().reduce(operator.mul).items
            Traceback (most recent call last):
                ...
            ValueError: nothing to reduce
            >>> C.erase().reduce(operator.mul, init=lambda: 0).items
            [0]
            >>> C.reduce(operator.mul, init=lambda: 5).items
            [5000]
        """
        if not self.items:
            if not init:
                raise ValueError("nothing to reduce")
            return self._clone([init()])
        todo = self.items[:]
        if init is None:
            acc = todo.pop(0)
        else:
            acc = init()
        while todo:
            acc = func(acc, todo.pop(0))
        return self._clone([acc])

    def join_by(self: ColT,
                dlm: str = '',
                pfx: str = '',
                sfx: str = '',
                ) -> ColT:
        """
        Emit collection of single string created by joining items

        Example:

            >>> C = Col("Joe Anne Bob Jane Betty".split())
            >>> C.join_by(dlm=", ", pfx="I say: ", sfx="--hey!").items
            ['I say: Joe, Anne, Bob, Jane, Betty--hey!']
        """
        joint = pfx + self.reduce(lambda a, b: dlm.join([a, b])).items[0] + sfx
        return self._clone([joint])


class _EndOfCollection:
    pass


def _mkempty():
    """
    Empty generator maker
    """
    return
    yield


class Gen(Generic[ItemT]):

    """
    A generator wrapper that adds several methods that allow to easily chain
    operations into pipelines as described in Martin Fowler's Collection
    Pipeline article:

        https://www.martinfowler.com/articles/collection-pipeline/#FirstEncounters

    Every method will return new instance of Gen with items altered as needed.
    This allows for easy chaining of collections.

    Gen() itself does implement iteration but for convenience, items can be
    retrieved as list using *items* property.

    Properties *first*, *last* and *only* can be used as "hidden" asserts:
    they are guarranteed to return single collection members but raise
    ScalarError if the collection itself is empty or -- in case of *only*,
    if it has more than one element.

    For example:

        >>> L = [1, 2, 3, 100, 200, 300]
        >>> Gen(L).foreach(str).items
        ['1', '2', '3', '100', '200', '300']
        >>> Gen(L).foreach(str).foreach(tuple).column(0).join_by('.').only
        '1.2.3.1.2.3'
        >>> Gen(L).foreach(str).select(lambda s: s.endswith('0')).last
        '300'
        >>> Gen([-5, None, 5]).default(lambda: 0).items
        [-5, 0, 5]
    """

    class _Buffer:

        def __init__(self, src):
            self._src_iter = src._src_iter

        def __next__(self):
            return next(self._src_iter)

    def __init__(self: GenT,
                 items: ItemGenT,
                 debugfn: Optional[DebugFnT] = None,
                 _src_iter: Optional[Iterator] = None,
                 ) -> None:
        self._EOC = _EndOfCollection()
        if _src_iter is None:
            self._src_iter = iter(items)
        else:
            self._src_iter = _src_iter
        self._debugfn = debugfn or debug

    def __iter__(self):
        return self._Buffer(self)

    def _clone(self: GenT,
               mkgen: Callable,
               *args,
               **kwargs,
               ) -> GenT:
        newiter = mkgen(*args, **kwargs)
        return self.__class__(
            items=_mkempty(),
            debugfn=self._debugfn,
            _src_iter=newiter,
        )

    def _all(self: GenT) -> GenT:
        return self.__class__(
            items=_mkempty(),
            debugfn=self._debugfn,
            _src_iter=self._src_iter,
        )

    #
    # retrieval
    #

    @property
    def as_dict(self: GenT) -> dict:
        """
        Return all items as a dictionary

        Example:

            >>> Gen('abc').foreach(lambda i: (i, i.upper())).as_dict
            {'a': 'A', 'b': 'B', 'c': 'C'}
        """
        return dict(i for i in self._src_iter)

    @property
    def as_list(self: GenT) -> list:
        """
        Return all items as a list

        Example:

            >>> Gen('abc').foreach(str.upper).as_list
            ['A', 'B', 'C']
        """
        return [i for i in self._src_iter]

    @property
    def as_set(self: GenT) -> set:
        """
        Return all items converted to a set

        Example:

            >>> S = Gen('abca').as_set
            >>> type(S)
            <class 'set'>
            >>> sorted(S)
            ['a', 'b', 'c']
        """
        return set(i for i in self._src_iter)

    @property
    def as_tuple(self: GenT) -> tuple:
        """
        Return all items as a tuple

        Example:

            >>> Gen('abc').foreach(str.upper).as_tuple
            ('A', 'B', 'C')
        """
        return tuple(i for i in self._src_iter)

    @property
    def items(self: GenT) -> list:
        """
        Alias to Gen.as_list.

        Example:

            >>> Gen('abc').foreach(str.upper).items
            ['A', 'B', 'C']
        """
        return [i for i in self._src_iter]

    #
    # inspection
    #

    def debug(self: GenT,
              tag: str = '',
              fn: Optional[DebugFnT] = None,
              ) -> GenT:
        """
        For every item, emit debug message using fn or self.debugfn

        Example:

            >>> G = Gen("foo bar baz".split(), debugfn=print)
            >>> G.debug('THREEs').items
            THREEs|'foo'
            THREEs|'bar'
            THREEs|'baz'
            ['foo', 'bar', 'baz']
        """
        def gen(fn):
            for item in self._src_iter:
                fn('%s|%r' % (tag, item))
                yield item
        return self._clone(gen, fn=fn or self._debugfn)

    def validate(self: GenT,
                 fn: BoolFnT,
                 fmt: Optional[FormatFnT] = None,
                 ) -> GenT:
        """
        Emit collection composed of identical items or raise ValidationError.

        If for any item, fn returns False, raise ValidationError with the given
        item.  The argument to the exception is the "bad" item itself, unless
        *fmt* callable is provided; in that case the ValidationError will
        hold `fmt(item)`.

        Example:

            >>> NUMBERS = [3, 2, 1]
            >>> Gen(NUMBERS).validate(fn=lambda i: i<10).items
            [3, 2, 1]
            >>> Gen(NUMBERS).validate(fn=lambda i: i>1).items
            Traceback (most recent call last):
                ...
            cpi.ValidationError: 1
            >>> Gen(NUMBERS).validate(
            ...     fn=lambda i: i>1,
            ...     fmt=lambda i: "too low: %d" % i
            ... ).items
            Traceback (most recent call last):
                ...
            cpi.ValidationError: too low: 1
        """
        def gen(fmt):
            for item in self._src_iter:
                if fn(item):
                    yield item
                    continue
                raise ValidationError(fmt(item))
        return self._clone(gen, fmt=fmt or (lambda i: i))

    #
    # scalars
    #

    @property
    def first(self: GenT) -> ItemT:
        """
        Return the first member of collection or raise ScalarError

        If the collection is empty, a ScalarError is raised with *col*
        attribute set to refer to the collection.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.first
            1
            >>> E = Gen([])
            >>> E.first
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        for item in self._src_iter:
            return item
        raise ScalarError(message="item list is empty", col=self)

    @property
    def last(self: GenT) -> ItemT:
        """
        Return the last member of collection or raise ScalarError

        If the collection is empty, a ScalarError is raised with *col*
        attribute set to refer to the collection.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.last
            3
            >>> E = Gen([])
            >>> E.last
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        last = self._EOC
        for item in self._src_iter:
            last = item
        if isinstance(last, _EndOfCollection):
            raise ScalarError(message="item list is empty", col=self)
        return last

    @property
    def only(self: GenT) -> ItemT:
        """
        Return the only member of collection or raise ScalarError

        If the number of items in the collection is different than one,
        a ScalarError is raised with *col* attribute set to the current
        collection, so that it can be re-raised or dealt with depending
        on actual number of items.

        Example:

            >>> SINGLETON = [1]
            >>> Gen(SINGLETON).only
            1
            >>> Gen(SINGLETON).prepend(2).only
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is not a singleton: got 2 items so far
            >>> Gen(SINGLETON).erase().only
            Traceback (most recent call last):
                ...
            cpi.ScalarError: item list is empty
        """
        last = self._EOC
        got = 0
        for item in self._src_iter:
            if got:
                raise ScalarError(
                    "item list is not a singleton: got %d items so far"
                    % (got + 1),
                    col=self
                )
            last = item
            got += 1
        if isinstance(last, _EndOfCollection):
            raise ScalarError(message="item list is empty", col=self)
        return last

    #
    # slicing
    #

    def head(self: GenT, n: int = 1) -> GenT:
        """
        Emit collection of n first items

        Example:

            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.head(2).items
            [1, 2]
            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.head().items
            [1]
            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.head(INF).items
            [1, 2, 3, 4, 5, 6]
        """
        def gen():
            togo = n
            for item in self._src_iter:
                if togo < 1:
                    break
                yield item
                togo -= 1
        if n == INF:
            return self._all()
        return self._clone(gen)

    def tail(self: GenT,
             n: int = 1,
             ) -> GenT:
        """
        Emit collection of n last items

        Example:

            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.tail(2).items
            [5, 6]
            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.tail().items
            [6]
            >>> G = Gen([1, 2, 3, 4, 5, 6])
            >>> G.tail(INF).items
            [1, 2, 3, 4, 5, 6]
            >>> S = Gen([1])
            >>> S.tail(2).items
            Traceback (most recent call last):
                ...
            ValueError: not enough items: want tail(2), got 1
            >>> E = Gen([])
            >>> E.tail(2).items
            Traceback (most recent call last):
                ...
            ValueError: not enough items: want tail(2), got 0
        """
        def gen(win):
            got = 0
            if win:
                for item in self._src_iter:
                    got += 1
                    win.pop(0)
                    win.append(item)
                if win[0] == self._EOC:
                    raise ValueError(
                        "not enough items: want tail(%d), got %d"
                        % (n, got)
                    )
            for w in win:
                yield w
        if n == INF:
            return self._all()
        return self._clone(gen, [self._EOC] * n)

    #
    # chaining
    #

    def flatten(self: GenT) -> GenT:
        """
        Flatten all lists in self.item; return collection of longer item list.

        Example:

            >>> G = Gen([[1, 2], [3, 4], [5, 6]])
            >>> G.flatten().items
            [1, 2, 3, 4, 5, 6]
        """
        joint = self.reduce(func=lambda a, b: a+b, init=list).items
        return self.__class__(
            items=joint[0],
            debugfn=self._debugfn,
        )

    def append(self: GenT,
               item: ItemT,
               ) -> GenT:
        """
        Emit collection with extra item *item* appended at the end.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.append(4).items
            [1, 2, 3, 4]
        """
        def gen(newitem):
            for item in self._src_iter:
                yield item
            yield newitem
        return self._clone(gen, item)

    def prepend(self: GenT,
                item: ItemT,
                ) -> GenT:
        """
        Emit collection with extra item *item* pre-pended at the beginning.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.prepend(0).items
            [0, 1, 2, 3]
        """
        def gen(newitem):
            yield newitem
            for item in self._src_iter:
                yield item
        return self._clone(gen, item)

    def windows(self: GenT,
                size: int,
                pad: Optional[ItemT] = None,
                ) -> GenT:
        """
        Emit collection of *size* long slices of the original collection.

        Windows are padded using *pad* value so that the length of every
        window is guarranteed to be *size*.  If None already means something
        in your collection, you can set *pad* to something else, eg. col.VOID.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.windows(2).items
            [[1, 2], [2, 3], [3, None]]
            >>> G = Gen([1, 2, 3])
            >>> G.windows(0).items
            Traceback (most recent call last):
                ...
            ValueError: size must be at least 1, got: 0
            >>> G.windows(1).items
            [[1], [2], [3]]
            >>> G = Gen([None, 2, 3])
            >>> G.windows(2, pad=VOID).items
            [[None, 2], [2, 3], [3, <class 'cpi.cpi.VOID'>]]
        """

        def mkwin(size):
            win = []
            while len(win) < size:
                try:
                    win.append(next(self._src_iter))
                except StopIteration:
                    win.append(pad)
            return win

        def rpad(items, size):
            out = items.copy()
            while len(out) < size:
                out.append(pad)
            return out

        def gen(size, pad):
            win = mkwin(size)
            yield win
            for item in self._src_iter:
                win = win[1:] + [item]
                yield win
            rest = win[1:]
            for item in rest:
                win = rpad(rest, size)
                yield win

        if size < 1:
            raise ValueError(f"size must be at least 1, got: {size!r}")
        return self._clone(gen, size, pad)

    #
    # filtering
    #

    def column(self: GenT,
               idx: Any,
               ) -> GenT:
        # 'Any' because it could technically work with dict keys
        """
        Emit collection composed of sub-members of every item at index *idx*.

        Example:

            >>> G = Gen([(1, 2, 3), (100, 200, 300)])
            >>> G.column(1).items
            [2, 200]
            >>> G = Gen(["foo", "bar", "baz"])
            >>> G.column(2).items
            ['o', 'r', 'z']
        """
        def gen(idx):
            for item in self._src_iter:
                yield item[idx]
        return self._clone(gen, idx)

    def erase(self: GenT) -> GenT:
        """
        Emit empty collection

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.erase().items
            []
        """
        def gen(): return iter([])
        return self._clone(gen)

    def select(self: GenT,
               func: BoolFnT,
               ) -> GenT:
        """
        Emit collection of items where *func* evaluates to True

        Example:

            >>> G = Gen([1, 2, 3, 4])
            >>> G.select(func=lambda n: n > 2).items
            [3, 4]
        """
        def gen(func):
            for item in self._src_iter:
                if not func(item):
                    continue
                yield item
        return self._clone(gen, func)

    def tflt(self: GenT,
             pattern: tuple,
             asterisk: Any = ANY,
             ) -> GenT:
        """
        Emit collection of tuple-like items that match tuple pattern *pattern*.

        For every item of *pattern* tuple, respective item in self.items must
        be equal to the pattern item or to a special value of *cpi.ANY*.
        If *asterisk* is given, the provided value acts in place of *cpi.ANY*.

        Example:

            >>> data = []
            >>> data.append(("Joe", 36, "Seattle"))
            >>> data.append(("Jane", 23, "New York City"))
            >>> data.append(("Joe", 36, "Los Angeles"))
            >>> G = Gen(data)
            >>> G.tflt(("Joe", 36, ANY)).items
            [('Joe', 36, 'Seattle'), ('Joe', 36, 'Los Angeles')]
            >>> G = Gen(data)
            >>> G.tflt(("Joe", 36, None), asterisk=None).items
            [('Joe', 36, 'Seattle'), ('Joe', 36, 'Los Angeles')]
        """
        def match_cont(item, pattern):
            if len(item) != len(pattern):
                return False
            for idx in range(len(pattern)):
                if asterisk is None and pattern[idx] is None:
                    continue
                if pattern[idx] is asterisk:
                    continue
                if item[idx] == pattern[idx]:
                    continue
                return False
            return True

        def gen(pattern):
            for item in self._src_iter:
                if not match_cont(item, pattern):
                    continue
                yield item
        return self._clone(gen, pattern)

    def reject(self: GenT,
               func: BoolFnT,
               ) -> GenT:
        """
        Emit collection of items where *func* evaluates to False

        This is exact inverse of *Gen.select()*.

        Example:

            >>> G = Gen([1, 2, 3, 4])
            >>> G.reject(func=lambda n: n > 2).items
            [1, 2]
        """
        def gen(func):
            for item in self._src_iter:
                if func(item):
                    continue
                yield item
        return self._clone(gen, func)

    def dedup(self: GenT,
              fn: Optional[TranslateFnT] = None,
              ) -> GenT:
        """
        Emit collection unique items of this collection.

        If *fn* is provided, instead of comparing items by their value, they
        are compared by value of this function.

        Example:

            >>> G = Gen("foo.bar.baz.bar.foo".split('.'))
            >>> G.dedup().items
            ['foo', 'bar', 'baz']
            >>> G = Gen("foo.bar.baz.bar.foo".split('.'))
            >>> G.dedup(lambda i: i[1]).items
            ['foo', 'bar']
        """
        def gen(fn):
            seen = []
            uniq = []
            for item in self._src_iter:
                k = fn(item)
                if k in seen:
                    continue
                seen.append(k)
                uniq.append(item)
            for u in uniq:
                yield u
        return self._clone(gen, fn=fn or (lambda i: i))

    #
    # sorting and ordering
    #

    def sort(self: GenT,
             fn: Optional[TranslateFnT] = None,
             ) -> GenT:
        """
        Emit same items in order sorted acording to fn.

        Example:

            >>> G = Gen([3, 1, 2])
            >>> G.sort().items
            [1, 2, 3]
            >>> G = Gen("axolotl foo hello".split())
            >>> G.sort(fn=len).items
            ['foo', 'hello', 'axolotl']
        """
        def gen(fn):
            for item in sorted(self._src_iter, key=fn):
                yield item
        return self._clone(gen, fn)

    def reverse(self: GenT) -> GenT:
        """
        Emit same items in reversed order.

        Example:

            >>> G = Gen([1, 2, 3])
            >>> G.reverse().items
            [3, 2, 1]
        """
        def gen():
            ffub = [m for m in self._src_iter]
            while ffub:
                yield ffub.pop()
        return self._clone(gen)

    def group_by(self: GenT,
                 fn: TranslateFnT,
                 ) -> GenT:
        """
        Emit collection of lists, self.items grouped according to *fn(item)*.

        Example:

            >>> G = Gen("Joe Anne Bob Jane Betty".split())
            >>> G.group_by(lambda S: S[0]).items
            [['Joe', 'Jane'], ['Anne'], ['Bob', 'Betty']]
        """
        def gen(fn):
            grouped = collections.defaultdict(list)
            for item in self._src_iter:
                grouped[fn(item)].append(item)
            for lst in grouped.values():
                yield lst
        return self._clone(gen, fn)

    #
    # member altering
    #

    def foreach(self: GenT,
                func: VarargFnT,
                *args,
                **kwargs,
                ) -> GenT:
        """
        Emit collection with every item replaced by result of *func(item)*.

        Example:

            >>> NAMES = "Joe Anne Bob Jane Betty".split()
            >>> Gen(NAMES).foreach(str.lower).items
            ['joe', 'anne', 'bob', 'jane', 'betty']
            >>> Gen(NAMES).foreach(str.split, "n").items
            [['Joe'], ['A', '', 'e'], ['Bob'], ['Ja', 'e'], ['Betty']]
            >>> Person = collections.namedtuple('Person', 'name,age')
            >>> Gen(NAMES).head(2).foreach(Person, age=30).items
            [Person(name='Joe', age=30), Person(name='Anne', age=30)]
        """
        def gen():
            for item in self._src_iter:
                yield func(item, *args, **kwargs)
        return self._clone(gen)

    def default(self: GenT,
                factory: InitFnT,
                ) -> GenT:
        """
        Emit collection of items with None re-generated using *factory*

        Example:

            >>> G = Gen([3, 4, None, 6, 7])
            >>> G.default(lambda: 5).items
            [3, 4, 5, 6, 7]
        """
        def gen():
            def setdefault(i): return factory() if i is None else i
            for item in self._src_iter:
                yield setdefault(item)
        return self._clone(gen)

    #
    # folding
    #

    def reduce(self: GenT,
               func: FoldFnT,
               init: Optional[InitFnT] = None,
               ) -> GenT:
        """
        Emit collection of single item produced by folding on *func*

        First, *func* is called with first two items as positional arguments.
        Result of this call is then used for another call along with third
        item, etc. until the whole collection is exhausted.  Finally, return
        new collection with the last *func* result as its only item.

        If *init* is provided, it must be a function that accepts no arguments
        and the above process starts as if the first item in collection was the
        return value of *init*.

        If collection is empty, *init* result is used to construct the returned
        singleton collection.  If collection is empty and no *init* is given,
        ValueError is raised.

        Example:

            >>> import operator
            >>> TRIPLET = [1, 10, 100]
            >>> Gen(TRIPLET).reduce(operator.mul).items
            [1000]
            >>> Gen([]).reduce(operator.mul).items
            Traceback (most recent call last):
                ...
            ValueError: nothing to reduce
            >>> Gen([]).reduce(operator.mul, init=lambda: 0).items
            [0]
            >>> Gen(TRIPLET).reduce(operator.mul, init=lambda: 5).items
            [5000]
        """
        def gen():
            acc = self._EOC
            while True:
                if acc == self._EOC:
                    if init:
                        acc = init()
                    else:
                        try:
                            acc = next(self._src_iter)
                        except StopIteration:
                            raise ValueError("nothing to reduce")
                try:
                    b = next(self._src_iter)
                except StopIteration:
                    yield acc
                    return
                acc = func(acc, b)
        return self._clone(gen)

    def join_by(self: GenT,
                dlm: str = '',
                pfx: str = '',
                sfx: str = '',
                ) -> GenT:
        """
        Emit collection of single string created by joining items

        Example:

            >>> G = Gen("Joe Anne Bob Jane Betty".split())
            >>> G.join_by(dlm=", ", pfx="I say: ", sfx="--hey!").items
            ['I say: Joe, Anne, Bob, Jane, Betty--hey!']
        """
        def gen():
            joint = pfx
            do_dlm = False
            for item in self._src_iter:
                if do_dlm:
                    joint += dlm
                joint += item
                do_dlm = True
            joint += sfx
            yield joint
        return self._clone(gen)
